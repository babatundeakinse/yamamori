function menuOverlay() {
  var container = document.querySelector( 'div.container' ),
    triggerBttn = document.querySelector( '.ymenu' ),
    overlay = document.querySelector( 'div#ymenu' ),
    closeBttn = overlay.querySelector( 'button.overlay-close' );
    transEndEventNames = {
      'WebkitTransition': 'webkitTransitionEnd',
      'MozTransition': 'transitionend',
      'OTransition': 'oTransitionEnd',
      'msTransition': 'MSTransitionEnd',
      'transition': 'transitionend'
    },
    transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
    support = { transitions : Modernizr.csstransitions };

  function toggleOverlay() {
    if( classie.has( overlay, 'open' ) ) {
      classie.remove( overlay, 'open' );
      classie.remove( container, 'overlay-open' );
      classie.add( overlay, 'close-me' );
      var onEndTransitionFn = function( ev ) {
        if( support.transitions ) {
          if( ev.propertyName !== 'visibility' ) return;
          this.removeEventListener( transEndEventName, onEndTransitionFn );
        }
        classie.remove( overlay, 'close-me' );
      };
      if( support.transitions ) {
        overlay.addEventListener( transEndEventName, onEndTransitionFn );
      }
      else {
        onEndTransitionFn();
      }
    }
    else if( !classie.has( overlay, 'close-me' ) ) {
      classie.add( overlay, 'open' );
      classie.add( container, 'overlay-open' );
    }
  }

  triggerBttn.addEventListener( 'click', toggleOverlay );
  closeBttn.addEventListener( 'click', toggleOverlay );

  $('.ymenu').click(function(e) {
      e.preventDefault();
  });
}

 $("#banner").owlCarousel({
      lazyLoad: true,
      lazyEffect: "fade",
      singleItem: true,
      navigation: false,
      slideSpeed : 450,
      pagination: false,
      transitionStyle: "fade",
      theme: "owl-theme-featured"
 });

$('.menu-section-header').click(function(){
    $(this).css('border-bottom', 'none');
    console.log('here');
});

 $( '.menu-btn' ).click(function(){
  $('.responsive-menu').addClass('expand')
  $('.menu-btn').addClass('btn-none')
 });
 
 $( '.close-btn' ).click(function(){
  $('.responsive-menu').removeClass('expand')
  $('.menu-btn').removeClass('btn-none')
 });