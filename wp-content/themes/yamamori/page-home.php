<?php
/*
 * Template Name: Home Template
 * Description: For the Gateway Template or Landing Template
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-home.twig' ), $context );