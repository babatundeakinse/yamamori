<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		remove_action( 'init', 'wp_admin_bar_init' );
		add_action( 'after_setup_theme', 'add_post_formats', 20 );
		add_filter ("manage_edit-ym_events_columns", "ym_events_edit_columns");
		add_action ("manage_posts_custom_column", "ym_events_custom_columns");
		//add_filter( 'pre_option_rg_gforms_disable_css', '__return_true' );
		wp_enqueue_script('jquery');
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
		include 'includes/post_type.php';
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies

		$labels = array(
			'name' => _x( 'Categories', 'taxonomy general name' ),
			'singular_name' => _x( 'Category', 'taxonomy singular name' ),
			'search_items' =>  __( 'Search Categories' ),
			'popular_items' => __( 'Popular Categories' ),
			'all_items' => __( 'All Categories' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Category' ),
			'update_item' => __( 'Update Category' ),
			'add_new_item' => __( 'Add New Category' ),
			'new_item_name' => __( 'New Category Name' ),
			'separate_items_with_commas' => __( 'Separate categories with commas' ),
			'add_or_remove_items' => __( 'Add or remove categories' ),
			'choose_from_most_used' => __( 'Choose from the most used categories' ),
		);

		register_taxonomy('ym_eventcategory','ym_events', array(
			'label' => __('Event Category'),
			'labels' => $labels,
			'hierarchical' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'event-category' ),
		));
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('Main Menu');
		$context['fmenu'] = new TimberMenu('Footer Menu');
		$context['cmenu'] = new TimberMenu('Category Menu');
		$args =  array(
			'type' => 'monthly',
			'show_year' => true,
			'order' => 'DESC',
			'nested' => true ,
		);
		$context['garchives'] = new TimberArchives( $args );
		$context['site'] = $this;
		$context['options'] = get_field('branches', 'option');
		$context['global_option'] = get_fields('option');

		//var_dump($context['global_option']);

		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}


	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$getinstagram = new Twig_SimpleFunction('displayinstagram' , function($id=979323412 , $followtext="FOLLOW US @YAMAMORIDUBLIN"){ //default to restaurants feed

			$instagram = do_shortcode( '[instagram-feed id='. $id .' num=10 cols=5 showfollow=true followtext="'.$followtext.'"]' );
			return $instagram;
		});

		$gravityfunction = new Twig_SimpleFunction('displaygform', function ($id) {
			$form = gravity_form($id,false, false, false, array(), true);
			return $form;
		});

		$gettwitter = new Twig_SimpleFunction('displaytwitter' , function($username='yamamoridublin'){ //default to restaurants feed
			$twitter = do_shortcode( '[fts_twitter twitter_name='. $username .']' );
			return $twitter;
		});

		$getMixCloud = new Twig_SimpleFunction('displaymixcloud' , function($url){

			//var_dump($url);

			$mixcloud = do_shortcode( '[mixcloud height=300 width=100% ] '. $url .' [/mixcloud]' );

			//var_dump($mixcloud);

			return $mixcloud;
		});

		$getYouTube = new Twig_SimpleFunction('displayyoutube' , function($url){

			//var_dump($url);

			$youtube = do_shortcode( '[youtube '. $url .'&w=480&h=300 ]' );

			//var_dump($youtube);

			return $youtube;
		});

		$textTwitter = new Twig_SimpleFunction('singletwitter', function(){

			$twitter  =  do_shortcode('[display_tweets]');

			return $twitter;
		});

		$textSlug = new Twig_SimpleFunction('getstringslug', 'createSlug');

		$twig->addFunction($getinstagram);
		$twig->addFunction($gravityfunction);
		$twig->addFunction($gettwitter);
		$twig->addFunction($getMixCloud);
		$twig->addFunction($getYouTube);
		$twig->addFunction($textSlug);
		$twig->addFunction($textTwitter);

		return $twig;
	}

}

new StarterSite();

function createSlug($str)
{
	$str = strtolower($str);
	$str = str_replace(' ', '_', $str);

	$newstr = preg_replace('/[^a-zA-Z0-9\']/', '_', $str);
	$newstr = str_replace("'", '', $newstr);

	return $newstr;
}

function tweet_template($tweet) {

$tweet_array = array();
  global $tweet_array;
    global $count;
    $count++;
    $an = 'tw'.$count;
    //include "includes/tweet.php";
 //dd($tweet);
    $twitcol['text'] = makeClickableLinks($tweet->text);
    $twitcol['time'] = tweetTime($tweet->created_at);
    $twitcol['url'] = 'https://twitter.com/yamamoridublin/status/'.$tweet->id_str;

    $tweet_array[] = $twitcol;
}





function hextweet($number){
	global $tweet_array;
	$twc = array();
	$twc['type'] = 'tweet';
	$twc['text']  = $tweet_array[$number]['text'];
	$twc['time']  = $tweet_array[$number]['time'];
	$twc['icon'] = 'twitter';
	$twc['img'] = get_bloginfo('url').'/wp-content/uploads/social-tweet.jpg';
	$twc['link'] = $tweet_array[$number]['url'];
	$twc['target'] = 1;

	return $twc;
}


function tweetTime( $t ) {
    /**** Begin Time Loop ****/
    // Set time zone
    date_default_timezone_set('America/New_York');
    // Get Current Server Time
    $server_time = $_SERVER['REQUEST_TIME'];
    // Convert Twitter Time to UNIX
    $new_tweet_time = strtotime($t);
    // Set Up Output for the Timestamp if over 24 hours
    $this_tweet_day =  date('D M j, Y', strtotime($t));
    // Subtract Twitter time from current server time
    $time = $server_time - $new_tweet_time;
    // less than an hour, output 'minutes' messaging
    if( $time < 3599) {
        $time = round($time / 60) . ' minutes ago';
            }
    // less than a day but over an hour, output 'hours' messaging
    else if ($time >= 3600 && $time <= 86400) {
        $time = round($time / 3600) . ' hours ago';
        }
    // over a day, output the $tweet_day formatting
    else if ( $time > 86400)  {
        $time = $this_tweet_day;
        }
    // return final time from tweetTime()
    return $time;
    /**** End Time Loop ****/
}


function add_post_formats() {
	add_theme_support( 'post-formats', array( ) );
}

function ym_events_edit_columns($columns) {

	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => "Event",
		"ym_col_ev_desc" => "Description",
		"ym_col_ev_startdate" => "Start Dates",
		"ym_col_ev_enddate" => "End Dates",
		"ym_col_ev_cat" => "Category",
	);
	return $columns;
}


function ym_events_custom_columns($column)
{
	global $post;
	$startd = get_field('ym_events_startdate' , $post->ID);
	$endd = get_field('ym_events_enddate' , $post->ID);
	switch ($column)
	{
		case "ym_col_ev_cat":
			// - show taxonomy terms -
			$eventcats = get_the_terms($post->ID, "ym_eventcategory");
			$eventcats_html = array();
			if ($eventcats) {
				foreach ($eventcats as $eventcat)
					array_push($eventcats_html, $eventcat->name);
				echo implode($eventcats_html, ", ");
			} else {
				_e('None', 'themeforce');;
			}
			break;
		case "ym_col_ev_startdate":
			// - show dates -
			echo $startd;
			break;
		case "ym_col_ev_enddate":

			echo $endd;
			break;
		case "ym_col_ev_desc";
			the_excerpt();
			break;

	}
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}




// ALL STANDARD FIELDS

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5694c3ae6edf6',
	'title' => 'Website Options',
	'fields' => array (
		array (
			'key' => 'field_5694c49cfdb17',
			'label' => 'Vital',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5694c3b4fdb0d',
			'label' => 'Google Analytics ID',
			'name' => 'google_analytics_id',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c427fdb12',
			'label' => 'Webmaster Tools ID',
			'name' => 'webmaster_tools_id',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c3f4fdb11',
			'label' => 'Redirects Completed',
			'name' => 'redirects_completed',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'No' => 'No',
				'Yes' => 'Yes',
				'No need for 301 redirects' => 'No need for 301 redirects',
			),
			'default_value' => array (
				'No' => 'No',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_5694c3d2fdb10',
			'label' => 'Logo',
			'name' => 'logo',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_5694c493fdb16',
			'label' => 'Schema.org',
			'name' => 'schemaorg',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5694c468fdb14',
			'label' => 'Business Type',
			'name' => 'business_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				'LocalBusiness' => 'Local Business',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_5694c3c9fdb0f',
			'label' => 'Email',
			'name' => 'email',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c3befdb0e',
			'label' => 'Phone Number',
			'name' => 'phone_number',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c43ffdb13',
			'label' => 'Address Line 1',
			'name' => 'address_line 1',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630a77ee1b',
			'label' => 'Address Line 2',
			'name' => 'address_line_2',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630af7ee1c',
			'label' => 'Town / City',
			'name' => 'town_city',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630be7ee1d',
			'label' => 'County',
			'name' => 'county',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630c57ee1e',
			'label' => 'Postcode',
			'name' => 'postcode',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630cc7ee1f',
			'label' => 'Country',
			'name' => 'country',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569636cd494fd',
			'label' => 'Same As',
			'name' => 'same_as',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => 'Add Row',
			'sub_fields' => array (
				array (
					'key' => 'field_569637aaca6e4',
					'label' => 'Link',
					'name' => 'link',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
		array (
			'key' => 'field_569636f8494ff',
			'label' => 'Google Map',
			'name' => 'google_map',
			'type' => 'google_map',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'height' => '',
			'center_lat' => '',
			'center_lng' => '',
			'zoom' => '',
		),
		array (
			'key' => 'field_569631b9e87a7',
			'label' => 'Google Maps URL',
			'name' => 'google_maps_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c4b9fdb18',
			'label' => 'Social',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'left',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_5694c4c6fdb19',
			'label' => 'Facebook URL',
			'name' => 'facebook_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c4cdfdb1a',
			'label' => 'Twitter URL',
			'name' => 'twitter_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5694c4d2fdb1b',
			'label' => 'Google+ URL',
			'name' => 'google_plus_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630fa7ee20',
			'label' => 'LinkedIn URL',
			'name' => 'linkedin_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569630fd7ee21',
			'label' => 'Pinterest URL',
			'name' => 'pinterest_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569631087ee22',
			'label' => 'Tumblr URL',
			'name' => 'tumblr_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5696310f7ee23',
			'label' => 'Instagram URL',
			'name' => 'instagram_url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_569632b29889d',
			'label' => 'Development',
			'name' => 'development',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_569632bf9889e',
			'label' => 'jQuery in footer',
			'name' => 'jquery_in_footer',
			'type' => 'true_false',
			'instructions' => 'Load jQuery in the footer of the website?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 0,
			'message' => '',
		),
		array (
			'key' => 'field_569632de9889f',
			'label' => 'Override',
			'name' => 'override',
			'type' => 'true_false',
			'instructions' => 'Confirm everything is done to remove warnings and alerts!',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 0,
			'message' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

// Disable the plugin Instagram and Fluid video embeds from updates
function filter_plugin_updates( $value ) {
    unset( $value->response['instagram-feed/instagram-feed.php'] );
    unset( $value->response['fluid-video-embeds/fluid-video-embeds.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );