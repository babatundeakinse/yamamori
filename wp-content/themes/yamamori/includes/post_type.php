<?php

	//this is where you can register custom post types
	$labels = array(
		'name' => _x('Menu Section', 'post type general name'),
		'singular_name' => _x('Menu Section', 'post type singular name'),
		'add_new' => _x('Add New', 'menu item'),
		'add_new_item' => __('Add New Menu Section'),
		'edit_item' => __('Edit Menu Section'),
		'new_item' => __('New Menu Section'),
		'view_item' => __('View Menu Section'),
		'search_items' => __('Search Menu Section'),
		'not_found' =>  __('No events found'),
		'not_found_in_trash' => __('No events found in Trash'),
		'parent_item_colon' => '',
	);

	$args = array(
		'label' => __('Menu Section'),
		'labels' => $labels,
		'public' => false,
		'can_export' => true,
		'show_ui' => true,
		'_builtin' => false,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-feedback',
		'hierarchical' => true,
		'rewrite' => array( "slug" => "events" ),
		'supports'=> array('title' , 'page-attributes') ,
		'show_in_nav_menus' => true,
	);

	register_post_type( 'menu_section', $args);


	$labels = array(
		'name' => _x('Events', 'post type general name'),
		'singular_name' => _x('Event', 'post type singular name'),
		'add_new' => _x('Add New', 'events'),
		'add_new_item' => __('Add New Event'),
		'edit_item' => __('Edit Event'),
		'new_item' => __('New Event'),
		'view_item' => __('View Event'),
		'search_items' => __('Search Events'),
		'not_found' =>  __('No events found'),
		'not_found_in_trash' => __('No events found in Trash'),
		'parent_item_colon' => '',
	);

	$args = array(
		'label' => __('Events'),
		'labels' => $labels,
		'public' => true,
		'can_export' => true,
		'show_ui' => true,
		'_builtin' => false,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-calendar-alt',
		'hierarchical' => false,
		'rewrite' => array( "slug" => "events" ),
		'supports'=> array('title', 'thumbnail', 'excerpt', 'editor') ,
		'show_in_nav_menus' => true,
		'taxonomies' => array( 'ym_eventcategory', 'post_tag')
	);

	register_post_type( 'ym_events', $args);

	$labels = array(
		'name' => _x('Reviews', 'post type general name'),
		'singular_name' => _x('Review', 'post type singular name'),
		'add_new' => _x('Add New', 'reviews'),
		'add_new_item' => __('Add New Review'),
		'edit_item' => __('Edit Review'),
		'new_item' => __('New Review'),
		'view_item' => __('View Review'),
		'search_items' => __('Search Reviews'),
		'not_found' =>  __('No reviews found'),
		'not_found_in_trash' => __('No reviews found in Trash'),
		'parent_item_colon' => '',
	);

	$args = array(
		'label' => __('Reviews'),
		'labels' => $labels,
		'public' => true,
		'can_export' => true,
		'show_ui' => true,
		'_builtin' => false,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-format-status',
		'hierarchical' => false,
		'rewrite' => array( "slug" => "reviews" ),
		'supports'=> array('title', 'thumbnail', 'excerpt', 'editor') ,
		'show_in_nav_menus' => true
	);

	register_post_type( 'reviews', $args);

	$labels = array(
		'name' => _x('People', 'post type general name'),
		'singular_name' => _x('Person', 'post type singular name'),
		'add_new' => _x('Add New', 'people'),
		'add_new_item' => __('Add New Person'),
		'edit_item' => __('Edit Person'),
		'new_item' => __('New Person'),
		'view_item' => __('View Person'),
		'search_items' => __('Search Person'),
		'not_found' =>  __('No people found'),
		'not_found_in_trash' => __('No people found in Trash'),
		'parent_item_colon' => '',
	);

	$args = array(
		'label' => __('People'),
		'labels' => $labels,
		'public' => true,
		'can_export' => true,
		'show_ui' => true,
		'_builtin' => false,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-admin-users',
		'hierarchical' => false,
		'rewrite' => array( "slug" => "people" ),
		'supports'=> array('title') ,
		'show_in_nav_menus' => true
	);

	register_post_type( 'people', $args);
?>