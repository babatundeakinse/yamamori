// function togglescroll() {
//   $('body').on('touchstart', function(e) {
//     if ($('body').hasClass('noscroll')) {
//       e.preventDefault();
//     }
//   });
// }

jQuery(document).ready(function($){
  // togglescroll();

  // PRESS ESC KEY TO EXIT
  $(document).keydown(function(e) {
    if (e.keyCode == 27) {
      $(".mobilenav").fadeOut(500);
      $(".top-menu").removeClass("top-animate");
      $("body").removeClass("noscroll");
      $(".mid-menu").removeClass("mid-animate");
      $(".bottom-menu").removeClass("bottom-animate");
    }
  });

  $(".icon").click(function() {
    $(".mobilenav").fadeToggle(500);
    $(".top-menu").toggleClass("top-animate");
    $("body").toggleClass("noscroll");
    $(".mid-menu").toggleClass("mid-animate");
    $(".bottom-menu").toggleClass("bottom-animate");
  });

  $('#accordProfile').on('shown', function () {
     $(".icon-chevron-down").removeClass("icon-chevron-down").addClass("icon-chevron-up");
  });

  $('#accordProfile').on('hidden', function () {
     $(".icon-chevron-up").removeClass("icon-chevron-up").addClass("icon-chevron-down");
  });

  $('.menu-content.collapse')
    .on('show.bs.collapse hide.bs.collapse', function (e) {
      if ($(this).is(e.target)) {
        $(this).prev().toggleClass('in');
      }
    });
});