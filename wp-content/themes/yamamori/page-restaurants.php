<?php
/*
 * Template Name: Restaurants Template
 * Description: For the 3 Major Restuarants
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;


// WP_Query arguments
$args = array (
    'post_type' => array('post') ,
    'posts_per_page'         => '4',
    'meta_query'             => array(
        array(
            'key'       => 'priority',
            'value'     => '1',
        ),
    ),
    'meta_key' => 'priority',
    'orderby' => 'meta_value',
    'order' => 'ASC',
);

// The Query
query_posts( $args );
$context['priority'] = Timber::get_posts();

// WP_Query arguments
$args = array (
    'post_type' => array('post') ,
    'posts_per_page'         => '2',
    'meta_query'             => array(
        array(
            'key'       => 'priority',
            'value'     => '0',
        ),
    ),
    'meta_key' => 'priority',
    'orderby' => 'meta_value',
    'order' => 'ASC',
);

query_posts($args); // very important to have this part.
$context['posts'] = Timber::get_posts();

$sections_id = get_field('menu_list', $post->ID); // get all the sections attached to this post

// get pdf menu link from Custom Fields
$context['pdf_menu'] = get_field('downloadable', $post->ID);
// var_dump($sections_id);

// return;
// WP_Query arguments
if (!empty($sections_id)) {
    $args = array (
        'post_type' => array('menu_section') ,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post__in' =>  $sections_id,
        'posts_per_page' => 20
    );

    query_posts($args); // very important to have this part.
    $context['menu_listing'] = Timber::get_posts();
}
else {
    $context['menu_listing'] = array();
}


$people_id = get_field('people', $post->ID); // get all the sections attached to this post

// return;
// WP_Query arguments
if (!empty($people_id)) {
    $args = array (
        'post_type' => array('people') ,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post__in' =>  $people_id,
        'posts_per_page' => 4
    );

    query_posts($args); // very important to have this part.
    $context['people_list'] = Timber::get_posts();
}
else {
    $context['people_list'] = array();
}

Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-restaurants.twig' ), $context );