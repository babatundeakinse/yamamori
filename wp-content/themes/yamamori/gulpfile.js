var gulp         = require('gulp'),
    gutil        = require('gulp-util'),
    jshint       = require('gulp-jshint'),
    sass         = require('gulp-sass'),
    // concat       = require('gulp-concat'),
    sourcemaps   = require('gulp-sourcemaps'),
    minifycss    = require('gulp-minify-css'),
    autoprefixer = require('gulp-autoprefixer'),
    // uglify       = require('gulp-uglify'),
    plumber      = require('gulp-plumber'),
    notify       = require("gulp-notify"),
    browserSync  = require('browser-sync'),
    reload       = browserSync.reload,
    watchFiles   = 'assets/**/*',

    input  = {
      'sass': 'scss/styles.scss',
      'javascript': ['js/*.js']
    },

    output = {
      'stylesheets': 'assets/css',
      'javascript': 'assets/js'
    };

var reportError = function (error) {
    var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

    notify({
        title: 'Task Failed [' + error.plugin + ']',
        message: lineNumber + 'See console.',
        sound: 'Sosumi' // FOR WINDOWS CHECK THIS!!!! See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
    }).write(error);

    gutil.beep(); // Beep 'sosumi' again

    // Inspect the error object
    //console.log(error);

    // Easy error reporting
    //console.log(error.toString());

    // Pretty error reporting
    var report = '';
    var chalk = gutil.colors.white.bgRed;

    report += chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) { report += chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
    if (error.fileName)   { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }
    console.error(report);

    // Prevent the 'watch' task from stopping
    this.emit('end');
};


/* run the watch task when gulp is called without arguments */
gulp.task('default', ['watch']);

/* run javascript through jshint */
gulp.task('jshint', function() {
  return gulp.src(input.javascript)
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

/* compile scss files */
gulp.task('styles', function() {

  return gulp.src(input.sass)
    .pipe(plumber({ errorHandler: reportError }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(minifycss({compatibility: 'ie8'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.stylesheets))
    .pipe(browserSync.stream({match: '**/*.css'})); // Make sure this is called!
});

/* concat javascript files, minify if --type production
gulp.task('scripts', function() {
  return gulp.src(input.javascript)
  	.pipe(plumber({ errorHandler: reportError }))
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.js'))
      //only uglify if gulp is ran with '--type production'
    .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output.javascript))
    .pipe(browserSync.reload({stream:true})); // Make sure this is called!
});
*/

/* Prepare Browser-sync for localhost */
gulp.task('browser-sync', function() {
    browserSync({
        proxy: "dev.dgitup.com:8080/wordpress",
        files: ["**/*.php","**/*.twig"]
    });
});

/* Watch these files for changes and run the task on update */
gulp.task('watch', ['browser-sync'], function() {
    //gulp.watch(input.javascript, ['jshint', 'scripts']);
    gulp.watch(input.sass, ['styles']);
    // ignore sass for full reload
    gulp.watch([watchFiles, '!assets/css/styles.css'], reload);
});