<?php
/*
 * Template Name: Clubs Template
 * Description: For Tengus
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$today = date('Ymd');
// WP_Query arguments
$args = array (
    'post_type' => array('ym_events') ,
    'nopaging' => false,
    'meta_query' => array(
		array(
	        'key'		=> 'ym_events_startdate',
	        'compare'	=> '>=',
	        'value'		=> $today,
	    )/*,
	     array(
	        'key'		=> 'end_date',
	        'compare'	=> '<=',
	        'value'		=> $today,
	    )*/
    ),
    'orderby' => 'meta_value',
    'order' => 'ASC',
);

query_posts($args); // very important to have this part.

$context['events'] = Timber::get_posts();

$people_id = get_field('people_list', $post->ID); // get all the sections attached to this post

// return;
// WP_Query arguments
if (!empty($people_id)) {
    $args = array (
        'post_type' => array('people') ,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post__in' =>  $people_id,
        'posts_per_page' => 4
    );

    query_posts($args); // very important to have this part.
    $context['people_list'] = Timber::get_posts();
}
else {
    $context['people_list'] = array();
}

//var_dump($context);
Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-clubs.twig' ), $context );